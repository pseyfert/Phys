#!/usr/bin/env python
# =============================================================================
# @file
# Mandatory file to keep Python 'package' for Phys/LoKiJets
#
#   This file is a part of LoKi project -
#     "C++ ToolKit  for Smart and Friendly Physics Analysis"
#
#  The package has been designed with the kind help from
#  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
#  contributions and advices from G.Raven, J.van Tilburg,
#  A.Golutvin, P.Koppenburg have been used in the design.
#
# @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
# @date 2008-11-09
# =============================================================================
"""
Mandatory file to keep Python 'package' for Phys/LoKiJets 

This file is a part of LoKi project -
\"C++ ToolKit  for Smart and Friendly Physics Analysis\"

The package has been designed with the kind help from
Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
contributions and advices from G.Raven, J.van Tilburg,
A.Golutvin, P.Koppenburg have been used in the design.
"""
# =============================================================================
__author__  = " Vanya BELYAEV Ivan.Belyaev@nikhef.nl "
__version__ = " CVS Tag $Name: not supported by cvs2svn $, version $Revision: 1.1 $ " 
# =============================================================================


# =============================================================================
# The END 
# =============================================================================
