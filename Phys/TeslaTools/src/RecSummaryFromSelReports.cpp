#include <algorithm>
#include <map>

#include "Event/HltSelReports.h"
#include "Event/RecSummary.h"
#include "HltDAQ/IReportConvert.h"

#include "GaudiAlg/Transformer.h"
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class RecSummaryFromSelReports RecSummaryFromSelReports.cpp
 *
 * \brief Algorithm that converts the RecSummary from the HltSelReports,
 *        to a RecSummary object.
 *
 * This algorithm is a trivial wrapper around
 * `IReportConvert::RecSummaryObjectFromSummary`.
 * This functionality was extracted from the TeslaReportAlgo.
 *
 */
class RecSummaryFromSelReports: public Gaudi::Functional::Transformer<
    LHCb::RecSummary(const LHCb::HltSelReports&)>
{

public:
  RecSummaryFromSelReports(const std::string& name, ISvcLocator* pSvcLocator)
    : Transformer(name, pSvcLocator,
      KeyValue{"InputHltSelReportsLocation",
               LHCb::HltSelReportsLocation::Default},
      KeyValue{"OutputRecSummaryLocation",
               LHCb::RecSummaryLocation::Default})
  {};

  LHCb::RecSummary
  operator()(const LHCb::HltSelReports& selReports) const override;

  StatusCode initialize() override;

private:
  IReportConvert* m_conv{nullptr};
}; 


LHCb::RecSummary
RecSummaryFromSelReports::operator()(const LHCb::HltSelReports& selReports) const
{
  LHCb::RecSummary recSummary;

  const auto* selReport = selReports.selReport("Hlt2RecSummary");
  if (selReport) {
    auto info = selReport->substructure()[0].target()->numericalInfo();
    m_conv->RecSummaryObjectFromSummary(&info, &recSummary);
    if (msgLevel(MSG::DEBUG)) {
      debug() << "Expected RecSummary HLTOS has ID (106): "
              << selReport->substructure()[0].target()->summarizedObjectCLID() << endmsg;
      debug() << "Global subdetector properties:" << recSummary << endmsg;
    }
  } else {
    Warning("Hlt2RecSummary report unavailable, global reconstruction "
            "information will not be resurrected in RecSummary.").ignore();
  }

  return recSummary;
}


StatusCode RecSummaryFromSelReports::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize();
  if (sc.isFailure()) return sc;

  m_conv = tool<IReportConvert>("ReportConvertTool", this);
  m_conv->setReportVersionLatest();  // Are we stepping on someone else?
  if ( !m_conv ) {
    return Error("Unable to retrieve the ReportConvertTool");
  }

  return sc;
}


DECLARE_COMPONENT( RecSummaryFromSelReports )