// Include files 

#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/SmartIF.h"
#include "GaudiKernel/IDataProviderSvc.h"

#include "Kernel/RelatedInfoNamed.h"

// local
#include "LoKi/Particles45.h"
#include "LoKi/ILoKiSvc.h"
//
LoKi::Particles::RelatedInfo::RelatedInfo 
( const std::string& location , 
  const short        index    , 
  const double       bad      ) 
  : LoKi::AuxFunBase( std::tie ( location , index , bad ) )
  , m_location ( location ) 
  , m_index    ( index    ) 
  , m_bad      ( bad      ) 
{}
//
LoKi::Particles::RelatedInfo::RelatedInfo 
( const std::string& location , 
  const std::string& variable , 
  const double       bad      ) 
  : LoKi::AuxFunBase( std::tie ( location , variable , bad ) )
  , m_location ( location ) 
  , m_bad      ( bad      ) 
{
  const auto index = RelatedInfoNamed::indexByName( variable ); 
  if ( index == RelatedInfoNamed::UNKNOWN )
  {
    Warning("RelatedInfo variable " + variable + " unknown"); 
  }
  m_index = index;
}
//
LoKi::Particles::RelatedInfo*
LoKi::Particles::RelatedInfo::clone() const 
{ return new LoKi::Particles::RelatedInfo(*this) ; }
//
// MANDATORY: the only one essential method 
LoKi::Particles::RelatedInfo::result_type 
LoKi::Particles::RelatedInfo::operator () 
  ( LoKi::Particles::RelatedInfo::argument p ) const 
{
  if ( UNLIKELY(!p) ) 
  {
    Error("Invalid particle, return ...") ;
    return -2000 ;
  }
  //
  if ( UNLIKELY( !m_table || !sameEvent() ) )
  {
    SmartIF<IDataProviderSvc> ds ( lokiSvc().getObject() ) ;
    SmartDataPtr<IMAP> data ( ds , m_location ) ;
    if ( UNLIKELY(!data) ) 
    {
      Warning( "No table at location " + m_location ) ;
      return -2000 ;
    } 
    m_table = data ;
    setEvent () ;  
  }
  //
  const auto& r = m_table->relations( p ) ;
  if ( UNLIKELY(r.empty()) )  
  {
    Warning ( "No entry for particle" ) ;
    return m_bad ; 
  }
  if ( UNLIKELY( 1 != r.size() ) ) 
  {
    Warning ( ">1 entry for particle" ) ;
    return m_bad ; 
  }
  const auto& m = r[0].to() ;
  //
  return m.info ( m_index , m_bad ) ;
}
/// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::RelatedInfo::fillStream( std::ostream& s ) const 
{
  return s << "RELINFO('"
           << m_location << "',"
           << m_index    << "," << m_bad << ")" ;
}
