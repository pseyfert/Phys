//-----------------------------------------------------------------------------
//
// bjet identification based on muons
//
//-----------------------------------------------------------------------------

#ifndef LOKITOPOTAG_H
#define LOKITOPOTAG_H 1

// ============================================================================
// Includes

// Kernel
#include "Kernel/ITriggerTisTos.h"

#include "Kernel/IJetTagTool.h"

// Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IHistogramSvc.h"


// ============================================================================
// Declaration
namespace LoKi
{
  class TopoTag
    : public virtual IJetTagTool
    , public GaudiTool
  {
    public:

      StatusCode initialize() override; // standard initialise
      StatusCode finalize() override;   // standard finalise

      // Method that calculates the tag weight of a jet
      bool calculateJetProperty
      (
        const LHCb::Particle *jet,
        std::map <std::string,double > &jetWeight
      ) override;

      // standard constructor
      TopoTag
      (
        const std::string &type,
        const std::string &name,
        const IInterface *parent
      );

    private:
      ITriggerTisTos* m_TriggerTisTosTool;
      std::string m_TLine; // algorithm mode - PtRel or IPSig

  };
}

#endif
