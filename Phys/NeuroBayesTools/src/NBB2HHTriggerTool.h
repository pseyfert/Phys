#ifndef NBB2HHTRIGGERTOOL_H
#define NBB2HHTRIGGERTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// plots
#include "GaudiAlg/IHistoTool.h"


// NeuroBayes (only on linux with gcc for the moment)
#ifdef HAVE_NEUROBAYES
#include "NeuroBayesExpert.hh"
#include "nb_param.hh"
#endif


/** @class NBB2HHTriggerTool NBB2HHTriggerTool.h
 *
 *
 *  @author Ulrich Kerzel
 *  @date   2011-02-16
 */
class NBB2HHTriggerTool : public extends1<GaudiTool,IParticleFilter>{
public:

  /** initialize tool */
  StatusCode initialize() override;

  /** finalize tool */
  StatusCode finalize() override {return GaudiTool::finalize();}

  /** performs the filtering based on the output of NeuroBayes
   *  @see IParticleFilter
   */
  bool operator()(const LHCb::Particle* p) const override;


  /** standard constructor
   *  @param type the actual tool type (?)
   *  @param name the tool instance name
   *  @param parent the tool parent
   */
  NBB2HHTriggerTool(const std::string& type, const std::string& name,
                    const IInterface* parent);

private:

  /// local
  bool getInputVar(const LHCb::Particle* particle) const;

  // properties
  std::vector<float>          m_Expertise;
  float                       m_NetworkCut;
  bool                        m_DoPrecuts;
  bool                        m_UsePID;
  bool                        m_DoPlot;

  // attributes
  const IDistanceCalculator*  m_DistCalc; ///< LoKi::DistanceCalculator
  int                         m_nVar;     ///< number of variables
  IHistoTool*                 m_HistoTool;


  float*   m_inArray;

#ifdef HAVE_NEUROBAYES
  Expert*  m_NBExpert;
#endif


};
#endif // NBB2HHTRIGGERTOOL_H
