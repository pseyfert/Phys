// ============================================================================
#ifndef LOKI_LOKIALGO_H 
#define LOKI_LOKIALGO_H 1
// ============================================================================
// Include files
// ============================================================================
// LoKiAlgo
// ============================================================================
#include "LoKi/Algo.h"
#include "LoKi/AlgoTypes.h"
#include "LoKi/Loop.h"
#include "LoKi/LoopObj.h"
#include "LoKi/PrintLoopDecay.h"
#include "LoKi/GetLoKiAlgo.h"
// ============================================================================
/** @file
 *
 *  This file is a part of LoKi project - 
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas, 
 *  contributions and advices from G.Raven, J.van Tilburg, 
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-03-23
 */
// ============================================================================
// The END 
// ============================================================================
#endif // LOKI_LOKIALGO_H
// ============================================================================
