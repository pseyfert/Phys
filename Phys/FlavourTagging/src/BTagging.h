#ifndef USER_BTAGGING_H
#define USER_BTAGGING_H 1

#include "Kernel/DaVinciAlgorithm.h"
#include "Kernel/IBTaggingTool.h"

/** @class BTagging BTagging.h
 *
 *  Algorithm to tag the B flavour
 *
 *  @author Marco Musy
 *  @date   02/10/2006
 */

class BTagging : public DaVinciAlgorithm
{

public:

  /// Standard constructor
  BTagging( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override;    ///< Algorithm execution

private:

  /// Run the tagging on the given location
  void performTagging(const std::string & location);

private:

  std::string m_TagLocation; ///< Location of tags

};

//=======================================================================//
#endif // USER_BTAGGING_H
