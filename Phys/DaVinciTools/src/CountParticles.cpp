// ============================================================================
// Include files
// ============================================================================
// DaVinciKernel
// ============================================================================
#include "Kernel/DaVinciAlgorithm.h"
// ============================================================================
/** @class CountParticles
 *  Count particles per PID. Useful for tests.
 *  @author Patrick Koppenburg
 *  @date 2010-01-06
 */
class CountParticles : public DaVinciAlgorithm
{
public:
  // ==========================================================================
  /// the standard execution of the algorithm
  StatusCode execute() override
  {
    // get the particles
    const LHCb::Particle::ConstVector& parts = this->i_particles();
    //
    setFilterPassed ( !parts.empty() ) ;
    //
    for ( LHCb::Particle::ConstVector::const_iterator i = parts.begin() ;
          i != parts.end(); ++i )
    {
      const LHCb::ParticleProperty * pp = ppSvc()->find((*i)->particleID());
      if ( pp ) { ++counter(pp->name()); }
    }
    //
    return StatusCode::SUCCESS ;
  }
  // ==========================================================================
  using DaVinciAlgorithm::DaVinciAlgorithm;
  // ==========================================================================
};
// ============================================================================
/// declare the factory (needed for instantiation)
DECLARE_COMPONENT( CountParticles )
// ============================================================================
// The END
// ============================================================================
