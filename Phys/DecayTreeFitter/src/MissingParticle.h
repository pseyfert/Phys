#ifndef VTK_MISSINGPARTICLE_HH
#define VTK_MISSINGPARTICLE_HH

#include "ParticleBase.h"

namespace DecayTreeFitter
{

  class MissingParticle : public ParticleBase
  {
  public:
    MissingParticle(const LHCb::Particle& bc, const ParticleBase* mother) ;
    virtual ~MissingParticle() ;

    ErrCode initPar1(FitParams*) override;
    ErrCode initPar2(FitParams*) override { return ErrCode::success ; }

    std::string parname(int index) const override;
    int dim() const  override { return hasMassConstraint() ? 3 : 4 ; }
    int momIndex() const override { return index() ; }
    bool hasEnergy() const override { return hasMassConstraint() ? false : true ; }
    int type() const override { return kMissingParticle ; }
    void addToConstraintList(constraintlist& /*alist*/, int /*depth*/) const override {}
  } ;

}
#endif
