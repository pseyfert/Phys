#ifndef PVTRACKREMOVER_H
#define PVTRACKREMOVER_H 1

// Include files
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IPVReFitter.h"

struct IPVOfflineTool;

/** @class PVTrackRemover PVTrackRemover.h
 *
 *  Remove weighted contribution of tracks from an LHCb::Particle
 *  to a given LHCb::RecVertex.
 *  This class is <b>temporary</b> and for testing purposes. The reFit
 *  method makes no sense and is not implemented.
 *
 *  @author Juan Palacios
 *  @date   2010-12-07
 */
class PVTrackRemover : public GaudiTool, virtual public IPVReFitter {

public:
  /// Standard constructor
  PVTrackRemover( const std::string& type,
                  const std::string& name,
                  const IInterface* parent );

  StatusCode initialize() override;

  StatusCode reFit(LHCb::VertexBase* PV) const override;

  StatusCode remove(const LHCb::Particle* particle,
                    LHCb::VertexBase* referencePV) const override;

private:

  std::string m_pvToolType;
  IPVOfflineTool* m_pvTool = nullptr;

};
#endif // PVTRACKREMOVER_H
