// Include files

// from LHCb
#include "Event/SwimmingReport.h"

// local
#include "SwimmingReportCloner.h"

//-----------------------------------------------------------------------------
// Implementation file for class : SwimmingReportCloner
//
// 2011-10-09 : Roel Aaij
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
SwimmingReportCloner::SwimmingReportCloner( const std::string& type,
                                            const std::string& name,
                                            const IInterface* parent )
  : base_class ( type, name , parent ) { }

//=============================================================================

LHCb::SwimmingReport* SwimmingReportCloner::operator() (const LHCb::SwimmingReport* report)
{
  return this->clone(report);
}

//=============================================================================

LHCb::SwimmingReport* SwimmingReportCloner::clone(const LHCb::SwimmingReport* report)
{
  return cloneKeyedContainerItem<Cloner>(report);
}

//=============================================================================

// Declaration of the Tool Factory
DECLARE_COMPONENT( SwimmingReportCloner )
