#ifndef MICRODST_CALOADCCLONER_H
#define MICRODST_CALOADCCLONER_H 1

#include "ObjectClonerBase.h"

#include <MicroDST/ICloneCaloAdc.h>            // Interface
#include <MicroDST/Functors.hpp>

// from LHCb
#include "Event/CaloAdc.h"

/** @class CaloAdcCloner CaloAdcCloner.h src/CaloAdcCloner.h
 *
 *  Clone an LHCb::CaloAdc.
 *
 *  @author Ricardo Vazquez Gomez
 *  @date   2017-06-15
 */

class CaloAdcCloner : public extends<ObjectClonerBase,ICloneCaloAdc>
{

public:

  /// Standard constructor
  CaloAdcCloner( const std::string& type,
                   const std::string& name,
                   const IInterface* parent);

  LHCb::CaloAdc* operator() (const LHCb::CaloAdc* adc) override;

private:

  LHCb::CaloAdc* clone(const LHCb::CaloAdc* adc);

private:

  typedef MicroDST::BasicCopy<LHCb::CaloAdc>   BasicCaloAdcCloner;

};

#endif // MICRODST_CALOADCCLONER_H
