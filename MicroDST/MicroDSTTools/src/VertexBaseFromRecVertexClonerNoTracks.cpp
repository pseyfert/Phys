// Include files

// from LHCb
#include "Event/RecVertex.h"

// local
#include "VertexBaseFromRecVertexClonerNoTracks.h"

//-----------------------------------------------------------------------------
// Implementation file for class : VertexBaseFromRecVertexClonerNoTracks
//
// 2007-12-05 : Juan PALACIOS
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VertexBaseFromRecVertexClonerNoTracks::
VertexBaseFromRecVertexClonerNoTracks( const std::string& type,
                                       const std::string& name,
                                       const IInterface* parent )
  : VertexBaseFromRecVertexCloner ( type, name, parent ) { }

//=============================================================================

LHCb::RecVertex*
VertexBaseFromRecVertexClonerNoTracks::clone( const LHCb::RecVertex* vertex )
{
  LHCb::RecVertex* vertexClone = VertexBaseFromRecVertexCloner::clone(vertex);
  if ( vertexClone ) { vertexClone->clearTracks(); }
  return vertexClone;
}

//=============================================================================

// Declaration of the Tool Factory
DECLARE_COMPONENT( VertexBaseFromRecVertexClonerNoTracks )

//=============================================================================
