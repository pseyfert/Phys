2017-07-06 Phys v23r5
========================================

Release for 2017 production
----------------------------------------

This version is released on the 2017-patches branch.
It is based on Gaudi v28r2, LHCb v42r5, Lbcom v20r5 and Rec v21r5 and uses LCG_88 with ROOT 6.08.06.

- MomentumScaling: propagate other parameters to scaling algorithm
  - See merge request !147
- Update MicroDST cloners with better Calo object support
  - See merge request !142
- Persist CaloReco information
  - See merge request !137
- Update how the hashing is performed to hopefully fix some false duplicates in S29
  - See merge request !143
- Extract decay descriptors from Selections
  - See merge request !144
- Remove versions from packages
  - See merge request !141
- Revert !56
  - See merge request !138
- fix StdLoosePi024e
  - See merge request !136
