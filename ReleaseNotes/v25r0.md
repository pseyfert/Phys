2018-03-07 Phys v25r0
=====================

Development release for 2018 data taking, prepared on the 2018-patches branch.
It is based on Gaudi v29r3, LHCb v44r0, Lbcom v22r0 and Rec v23r0 and uses LCG_93 with ROOT 6.12.06.

- Bug fix for ParticleVertexFitter.
  - See merge request lhcb/Phys!276
- New tool related to HEPDrone paper,see LHCBPS-1769.
  - See merge request lhcb/Phys!251
- Mask gcc 7.3 compilation warnings from Python and Boost, exposed with LCG_93.
  - See merge request lhcb/Phys!268
- LoKiPhys.Phys : add default argument(ISBASIC) for protoparticles function.
  - See merge request lhcb/Phys!263
- SelectionLine: remove unused function to fix clang warning.
  - See merge request lhcb/Phys!267
- VertexFitter: fix clang warnings.
  - See merge request lhcb/Phys!266
- Fix GCC7 strict-aliasing warnings.
  - See merge request lhcb/Phys!264
- Fix DTF refit issue.
  - See merge request lhcb/Phys!260
- Substitutions in DTFDict - Part II.
  - See merge request lhcb/Phys!253
- Add new (fast) particle vertex fitter.
  - See merge request lhcb/Phys!249
- Fix RestoreCaloRecoChain.
  - See merge request lhcb/Phys!255
- Modifications to DSTWriters needed for the "Stripping of Turbo" see JIRA WGP-6.
  - See merge request lhcb/Phys!231
- PhysSel/PhysSelPython & Phys/PhysConf.
  - See merge request lhcb/Phys!244
- Fix bug in CopyLinePersistenceLocations.
  - See merge request lhcb/Phys!236
- Add 2017 MC relations tables locations to TeslaTruthUtils
  - See merge request lhcb/Phys!237
- Allow for PersistReco-specific CaloCluster and CaloHypo cloners.
  - See merge request lhcb/Phys!233
- Cherry-pick MR !218 into 2018-patches
  - See merge request !221
- Remove unused member variable in DaVinciNeutralTools/src/RestoreCaloRecoChain.
  - See merge request !215
